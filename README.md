# Telegram Channel Comment Scraper

This script allows you to scrape comments from a forwarded post in a Telegram channel. It utilizes the Telegram API and Telegraf library for Node.js.

## Prerequisites

Before running the script, make sure you have the following:

- Telegram API credentials (API ID and API hash)
- Telegram bot token
- Node.js installed on your machine
- `dotenv` package installed (`npm install dotenv`)
- `telegraf` package installed (`npm install telegraf`)
- `telegram` package installed (`npm install telegram`)
- `input` package installed (`npm install input`)

## Installation

1. Clone this repository to your local machine.
2. Install dependencies using npm:
   `npm install`
3. Create a `.env` file in the root directory of the project and add your Telegram API credentials (https://core.telegram.org/api/obtaining_api_id) and bot token:
   `TELEGRAM_API_ID=your_api_id
TELEGRAM_API_HASH=your_api_hash
BOT_TOKEN=your_bot_token
CLIENT_SESSION_KEY=your_client_session_key`

Replace `your_api_id`, `your_api_hash`, `your_bot_token`, and `your_client_session_key` with your actual credentials.

## Usage

1. Run the script using Node.js:
   `node index.js`

2. Follow the instructions in the terminal to enter your phone number, password, and verification code (if necessary).
3. After authentication, the script will start listening for forwarded messages in the Telegram channel where the bot is added as an administrator.
4. Forward the message you want to scrape comments from to the bot.
5. The bot will process the request and reply with information about the channel, message ID, channel participants, commentators, and commentators among subscribers.
6. Response messages are comma-separated values. You can copy it to local file for further processing data in spreadsheets

## Notes

- This script may take some time to scrape comments, especially if there are many comments or participants in the channel.
- Make sure to add the bot as an administrator to the channel where you want to scrape comments.
- Be cautious with API usage to avoid rate limits or account suspension.
- Use this script responsibly and adhere to Telegram's terms of service.
