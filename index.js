const { Api, TelegramClient } = require("telegram");
const { StringSession } = require("telegram/sessions");
const { Telegraf } = require("telegraf");
const { message } = require("telegraf/filters");
const input = require("input");
const { all } = require("axios");

console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const apiId = process.env.TELEGRAM_API_ID;
const apiHash = process.env.TELEGRAM_API_HASH;
const botToken = process.env.BOT_TOKEN;
const clientSessionKey = process.env.CLIENT_SESSION_KEY;
const stringSession = new StringSession(clientSessionKey); // fill this later with the value from session.save()
const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));

(async () => {
  console.log("Loading interactive example...");

  const client = new TelegramClient(stringSession, apiId, apiHash, {
    consdanectionRetries: 5,
  });

  await client.start({
    phoneNumber: async () => await input.text("Please enter your number: "),
    password: async () => await input.text("Please enter your password: "),
    phoneCode: async () =>
      await input.text("Please enter the code you received: "),
    onError: (err) => console.log(err),
  });

  /* console.log(client.session.save()); // Save this string to avoid logging in again */

  try {
    await client.connect(); // This assumes you have already authenticated with .start()
    console.log("You should now be connected.");
  } catch (e) {
    console.log(e);
  }
  const removeDuplicates = (arr) => Array.from(new Set(arr));

  const getUserData = async (userId) => {
    if (userId !== undefined) {
      const result = await client.invoke(
        new Api.users.GetFullUser({
          id: userId,
        })
      );

      return `${result.users[0].lastName}, ${result.users[0].firstName}, ${result.users[0].username}, ${result.users[0].phone}`;
    }
  };
  /*
  const getChannelParticipants = async (client, chatId) => {
    const getParticipants = await client.invoke(
      new Api.channels.GetParticipants({
        channel: chatId,
        filter: new Api.ChannelParticipantsRecent({}),
        offset: 0,
        limit: 10000,
        hash: BigInt("-4156887774564"),
      })
    );

    return getParticipants;
  };
*/

  /* const getChannelParticipantsRequest = async (
    client,
    channelId,
    offset,
    limit
  ) => {
    const participants = await client.invoke(
      new Api.channels.GetParticipants({
        channel: channelId,
        filter: new Api.ChannelParticipantsSearch({ q: "" }),
        offset,
        limit,
        hash: 0,
      })
    );
    return participants.users;
  }; */

  const fetchParticipantsByQueryKeys = async (
    client,
    channelId,
    offset,
    limit
  ) => {
    /*const queryKeys = [
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
      "o",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z",
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "_",
    ];*/
    /*const queryKeys = Array.from({ length: 0x10ffff }, (_, i) =>
      String.fromCodePoint(i)
    );*/
    /* const queryKeys = [
      ...Array.from({ length: 10 }, (_, i) => String.fromCharCode(0x0030 + i)), // Цифры от 0 до 9
      ...Array.from({ length: 26 }, (_, i) => String.fromCharCode(0x0041 + i)), // Латинские заглавные буквы
      ...Array.from({ length: 26 }, (_, i) => String.fromCharCode(0x0061 + i)), // Латинские строчные буквы
      ...Array.from({ length: 32 }, (_, i) => String.fromCharCode(0x0410 + i)), // Кириллические заглавные буквы
      ...Array.from({ length: 32 }, (_, i) => String.fromCharCode(0x0430 + i)), // Кириллические строчные буквы
      "_", // Нижнее подчеркивание
      ...Array.from({ length: 128 }, (_, i) =>
        String.fromCodePoint(0x4e00 + i)
      ), // Иероглифические алфавиты
      ...Array.from({ length: 128 }, (_, i) =>
        String.fromCodePoint(0x1f600 + i)
      ), // Эмодзи
      ...Array.from({ length: 10 }, (_, i) => String.fromCharCode(0x0600 + i)), // Арабские цифры
      ...Array.from({ length: 27 }, (_, i) => String.fromCharCode(0x0621 + i)), // Арабские буквы
      ...Array.from({ length: 27 }, (_, i) => String.fromCharCode(0x05d0 + i)), // Ивритские буквы
      ...Array.from({ length: 32 }, (_, i) => String.fromCodePoint(0x0901 + i)), // Индийские буквы
    ];
*/
    const queryKeys = [
      ...Array.from({ length: 10 }, (_, i) => String.fromCharCode(0x0030 + i)), // Цифры от 0 до 9
      ...Array.from({ length: 26 }, (_, i) => String.fromCharCode(0x0041 + i)), // Латинские заглавные буквы
      ...Array.from({ length: 26 }, (_, i) => String.fromCharCode(0x0061 + i)), // Латинские строчные буквы
      ...Array.from({ length: 32 }, (_, i) => String.fromCharCode(0x0410 + i)), // Кириллические заглавные буквы
      ...Array.from({ length: 32 }, (_, i) => String.fromCharCode(0x0430 + i)), // Кириллические строчные буквы
      "_", // Нижнее подчеркивание
    ];
    const results = [];
    for (const key of queryKeys) {
      await sleep(1000);
      try {
        const participants = await client.invoke(
          new Api.channels.GetParticipants({
            channel: channelId,
            filter: new Api.ChannelParticipantsSearch({ q: key }),
            offset,
            limit,
            hash: 0,
          })
        );

        results.push(participants);
      } catch (error) {
        console.error("Error fetching participants:", error);
      }
    }
    return results;
  };

  const getChannelParticipantsRequest = async (
    client,
    channelId,
    offset,
    limit
  ) => {
    return fetchParticipantsByQueryKeys(client, channelId, offset, limit);
  };

  const extractParticipantsData = (array) => {
    console.log("extractParticipantsData input array", array.length);
    let resultArray = [];
    const arrCopy = array;
    arrCopy.map((item) => {
      const itemData = `${item.id}, ${item.lastName}, ${item.firstName}, ${item.username}\n`;

      resultArray.push(itemData);
    });

    return resultArray;
  };

  const getChannelMembers = async (client, channelId) => {
    const offset = 0;
    const limit = 201;

    const participantsResponse = await getChannelParticipantsRequest(
      client,
      channelId,
      offset,
      limit
    );

    const participants = participantsResponse;
    const allUsers = participants.flatMap((channel) => {
      return channel.users;
    });
    return allUsers;
  };

  /*  console.log(
    await getChannelParticipantsRequest(client, -1001333266122, 0, 200)
  );
*/
  /* const getChannelParticipants = async (client, channelId) => {
    let offset = 0;
    const limit = 201;
    const allParticipants = [];

    while (true) {
      const participants = await getChannelParticipantsRequest(
        client,
        channelId,
        offset,
        limit
      );
      console.log(participants.length);
      if (!participants.length) break;

      allParticipants.push(...participants.users);
      offset += participants.length;

      // Чтобы не перегрузить сервер, добавим небольшую задержку перед следующим запросом
      await sleep(1000);
    }

    return allParticipants;
  }; */

  const getReplies = async (client, peer, msgId) => {
    const result = await client.invoke(
      new Api.messages.GetReplies({
        peer: peer,
        msgId: msgId,
        offsetId: 0,
        offsetDate: 0,
        addOffset: 0,
        limit: 100000,
        maxId: 0,
        minId: 0,
        hash: BigInt("-4156887774564"),
      })
    );
    const replies = result.messages;
    return replies;
  };

  const extractRepliesData = async (array) => {
    let resultArray = [];
    const arrCopy = array;

    for (const item of arrCopy) {
      const date = new Date(item.date * 1000);
      const formattedDate = date.toLocaleString("en-US");

      if (item.fromId !== null) {
        if (item.fromId.userId == undefined) {
          const channelId = item.fromId.channelId;
          const itemData = `${channelId}, channel, ${formattedDate}, ${item.message}`;

          resultArray.push(itemData);
        }

        if (item.fromId.userId !== undefined) {
          await sleep(2600);
          const userData = await getUserData(item.fromId.userId);
          const itemData = `${item.fromId.userId}, user, ${formattedDate}, ${userData}, ${item.message}`;
          /* const itemData = `${item.fromId.userId}, user, ${formattedDate}, ${item.message}`; */

          resultArray.push(itemData);
        }
      }

      if (item.fromId == null) {
        const itemData = `null, null, ${formattedDate}, ${item.message}`;
        resultArray.push(itemData);
      }
    }
    console.log("extractRepliesData resultArray", resultArray);
    return resultArray;
  };

  const filterCommentators = (commentators, channelMembers) => {
    const channelMemberIds = channelMembers.map((member) => {
      return member.split(",")[0].trim();
    });

    return commentators.filter((commentator) => {
      const id = commentator.split(",")[0].trim();
      return channelMemberIds.includes(id);
    });
  };

  const sendLongMessage = async (ctx, message) => {
    try {
      const maxMessageLength = 4096;
      console.log("message", message);
      if (!message) {
        throw new Error("Message is undefined or empty.");
      }

      // Split the message into chunks of length not exceeding 4096 characters
      const chunks = [];
      let currentChunk = "";

      for (const line of message.split("\n")) {
        if ((currentChunk + line).length > maxMessageLength) {
          chunks.push(currentChunk);
          currentChunk = line + "\n";
        } else {
          currentChunk += line + "\n";
        }
      }
      chunks.push(currentChunk);

      // Send each chunk of the message
      for (const chunk of chunks) {
        await ctx.replyWithHTML(`<code>${chunk}</code>`);
      }
    } catch (error) {
      console.error("Error sending message:", error);
    }
  };

  const screenReservedCharacters = (str) => {
    let result = str;
    const regex = /([().{}.<>.!.-.+.&.=.#.~|])/g;
    result = result.replace(regex, "\\$1");
    return result;
  };

  const replaceBrackets = (str) => {
    const copyStr = str;
    const result = copyStr.replaceAll(">", "&gt;").replaceAll("<", "&lt;");
    return result;
  };

  const bot = new Telegraf(botToken, {
    handlerTimeout: 9_000_000,
  });

  bot.start((ctx) =>
    ctx.reply(
      "Hello! I will help you get a list of comments to a forwarded post. Just don't forget to do couple of things:\n1. Add me and @tgccsprovider as an administrator to the channel\n2. Forward the necessary post to me\n\nIf you don't trust this bot and provider account you can run your own instance - https://gitlab.com/yungscientist/telegram-comments-analyzer"
    )
  );

  bot.on(message("text"), async (ctx) => {
    await ctx.replyWithMarkdownV2(
      screenReservedCharacters("⏳ Processing your request...")
    );

    if (ctx.update.message.forward_origin == undefined) {
      ctx.replyWithMarkdownV2("This message is not forwarded");
    } else {
      if (ctx.update.message.forward_origin.type == "user") {
        try {
          ctx.replyWithMarkdownV2(
            "This message is forwarded not from a channel"
          );
        } catch (e) {
          console.log(e);
        }
      }
      if (ctx.update.message.forward_origin.type == "channel") {
        try {
          const chatId = ctx.update.message.forward_origin.chat.id;
          const msgId = ctx.update.message.forward_origin.message_id;

          const participants = await getChannelMembers(client, chatId);
          const channelMembers = await removeDuplicates(
            extractParticipantsData(participants)
          );

          const channelMembersInCsv = channelMembers.join("");

          const replies = await getReplies(client, chatId, msgId);
          const commentators = await extractRepliesData(replies);
          const commentatorsInCsv = (await extractRepliesData(replies)).join(
            "\n"
          );
          const filteredCommentators = filterCommentators(
            commentators,
            channelMembers
          );

          await ctx.replyWithMarkdownV2(`ℹ️ *Channel information*`);
          console.log(ctx.update.message.forward_origin.chat);
          await ctx.replyWithMarkdownV2(
            `\`\\${ctx.update.message.forward_origin.chat.id}, ${ctx.update.message.forward_origin.chat.title},
${ctx.update.message.forward_origin.chat.username}, ${channelMembers.length}\``
          );
          await ctx.replyWithMarkdownV2(`ℹ️ *Message ID*`);
          await ctx.replyWithMarkdownV2(
            `\`${ctx.update.message.forward_origin.message_id}\``
          );

          const filteredCommentatorsInCsv = filteredCommentators.join("\n");
          await ctx.replyWithMarkdownV2("👥 *Channel subscribers*");
          await sendLongMessage(ctx, channelMembersInCsv);

          // console.log("👥 Channel subscribers", channelMembersInCsv);
          await ctx.replyWithMarkdownV2("💬 *Commentators*");
          // console.log("commentatorsInCsv", commentatorsInCsv);
          await sendLongMessage(ctx, commentatorsInCsv);
          // console.log("💬 Commentators", commentatorsInCsv);

          await ctx.replyWithMarkdownV2("✅ *Commentators among subscribers*");
          /* console.log("filteredCommentators", filteredCommentatorsInCsv); */
          filteredCommentators.length > 0
            ? await sendLongMessage(ctx, filteredCommentatorsInCsv)
            : await ctx.reply("There is no commentators among subscribers");

          console.log(
            "✅ Commentators among subscribers",
            replaceBrackets(filteredCommentatorsInCsv)
          );
        } catch (e) {
          console.log("error", e);
          await ctx.replyWithHTML(
            `❌ ${replaceBrackets(
              "<bold>Error!</bold>"
            )}\n ${screenReservedCharacters(JSON.stringify(e))}`
          );
        }
      }
    }
  });

  bot.launch();
  process.once("SIGINT", () => bot.stop("SIGINT"));
  process.once("SIGTERM", () => bot.stop("SIGTERM"));
})();
